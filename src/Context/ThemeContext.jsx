import React from 'react';
export const Themes = {
    light: {
        foreground: 'text-dark',
        background: 'bg-light',
      },
      dark: {
        foreground: 'text-light',
        background: 'bg-dark',
      },
}

export const ThemeContext = React.createContext({theme:Themes.light,toggleTheme:() => {}});